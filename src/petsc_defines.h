#ifndef PETSC_DEFINES_H
#define PETSC_DEFINES_H

#include "cctk.h"

#include <petsc.h>
#include <petscversion.h>



/* The PETSc API changes frequently, sometimes even in minor releases.
   The #defines below map calls to the PETSc linear solver to the
   corresponding PETSc call (or sequence of calls), depending on the
   version of PETSc.  */



#if PETSC_VERSION_MAJOR < 2 ||                                  \
  (PETSC_VERSION_MAJOR == 2 && PETSC_VERSION_MINOR == 0)

/* Version 2.0.0 and higher */
#  include <sles.h>

#elif PETSC_VERSION_MAJOR == 2 && PETSC_VERSION_MINOR == 1

/* Version 2.1.0 and higher */
#  include <petscsles.h>

#elif PETSC_VERSION_MAJOR == 2 && PETSC_VERSION_MINOR == 2 && PETSC_VERSION_SUBMINOR == 0

/* Version 2.2.0 */
#  include <petscmat.h>
#  include <petscksp.h>
#  define SLES               KSP
#  define SLESCreate         KSPCreate
#  define SLESSetOperators   KSPSetOperators
#  define SLESGetKSP(a,b)    ((*b) = (a), 0)
#  define SLESGetPC          KSPGetPC
#  define SLESSetFromOptions KSPSetFromOptions
#  define SLESSolve(a,b,c,d) (KSPSetRhs(a,b),                   \
                              KSPSetSolution(a,c),              \
                              KSPSolve(a),                      \
                              KSPGetIterationNumber(a,d))
#  define SLESDestroy        KSPDestroy

#elif PETSC_VERSION_MAJOR == 2 && PETSC_VERSION_MINOR == 2

/* Version 2.2.1 and higher */
#  include <petscmat.h>
#  include <petscksp.h>
#  define SLES               KSP
#  define SLESCreate         KSPCreate
#  define SLESSetOperators   KSPSetOperators
#  define SLESGetKSP(a,b)    ((*b) = (a), 0)
#  define SLESGetPC          KSPGetPC
#  define SLESSetFromOptions KSPSetFromOptions
#  define SLESSolve(a,b,c,d) (KSPSolve(a,b,c),                  \
                              KSPGetIterationNumber(a,d))
#  define SLESDestroy        KSPDestroy

#else

/* Version 2.3.0 and higher */
#  include <petscmat.h>
#  include <petscksp.h>
#  define SLES               KSP
#  define SLESCreate         KSPCreate
#  define SLESSetOperators   KSPSetOperators
#  define SLESGetKSP(a,b)    ((*b) = (a), 0)
#  define SLESGetPC          KSPGetPC
#  define SLESSetFromOptions KSPSetFromOptions
#  define SLESSolve(a,b,c,d) (KSPSolve(a,b,c),                  \
                              KSPGetIterationNumber(a,d))
#  define SLESDestroy        KSPDestroy
#  define PetscSetCommWorld(a) (PETSC_COMM_WORLD = (a), 0)

#endif

#endif /* #ifndef PETSC_DEFINES_H */
